module Jekyll
	module RandomFilters
		# Get random item from array
		def random(array)
			array[rand(array.length)]
		end
		# Pop random item from array (same like previous filter, but remove item from array)
		def random_pop(array)
			array.delete_at(rand(array.length))
		end
	end
end

Liquid::Template.register_filter(Jekyll::RandomFilters)
