Všechny změny jsou připravovány ve větvi (branch), která bude pomocí merge
requestu "mergenuta" do větve master (jakmile jsme se změnami ve větvi všichni
spokojeni).

# Jak pojmenovávat větve
Pro naplánovaný úkol se obvykle založí issue a podle jejího čísla a názvu se pak
pojmenuje větev, např. `123-pridat-loga-sponzoru`. Pokud změna nemá issue, tak
jí stačí v pár slovech rozumně pojmenovat a větev tak nazvat (vždy malými
písmeny a s pomlčkami).

# Commit message
Každý commit je potřeba rozumně popsat, aby bez jeho otevření bylo zřejmé co za
změny v něm jsou.

# Jak funguje buildování
Pro každou větev se automaticky pro poslední commit sestaví web Jekyllem,
zkontroluje pomocí HTML Proofer ([co se kontroluje](https://github.com/gjtorikian/html-proofer#whats-tested))
a nasadí na nazev-vetve.ovacopicz.ci.poboril.cz (např. [master zde](http://master.ovacopicz.ci.poboril.cz/)).

V merge requestu je nahoře vidět stav buildu (sestavení) dané větve. Pokud se
build povede, tak se nasadí, pokud ne (fail), tak zůstane nasazená stará verze a
autorovi commitu se pošle upozornění mailem. Proč se build nepovedl je možné
vyčíst po rozkliknutí buildu.
